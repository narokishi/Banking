@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ Lang::get('form.register') }}</div>
                <div class="card-body">
                    {{ Form::open(['route' => 'user.store']) }}
                        <div class="form-group row">
                            {{ Form::label('first_name', Lang::get('form.first_name'), [
                                'class' => 'col-md-4 col-form-label text-md-right'
                            ]) }}
                            <div class="col-md-6">
                                {{ Form::text('first_name', null, [
                                    'class'    => 'form-control',
                                    'required' => 'required',
                                ]) }}
                                @if ($errors->has('first_name'))
                                    <span class="error text-danger">
                                        <strong>{{ $errors->first('first_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('last_name', Lang::get('form.last_name'), [
                                'class' => 'col-md-4 col-form-label text-md-right'
                            ]) }}
                            <div class="col-md-6">
                                {{ Form::text('last_name', null, [
                                    'class'    => 'form-control',
                                    'required' => 'required',
                                ]) }}
                                @if ($errors->has('last_name'))
                                    <span class="error text-danger">
                                        <strong>{{ $errors->first('last_name') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('email', Lang::get('form.email'), [
                                'class' => 'col-md-4 col-form-label text-md-right'
                            ]) }}
                            <div class="col-md-6">
                                {{ Form::text('email', null, [
                                    'class'    => 'form-control',
                                    'required' => 'required',
                                ]) }}
                                @if ($errors->has('email'))
                                    <span class="error text-danger">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('password', Lang::get('form.password'), [
                                'class' => 'col-md-4 col-form-label text-md-right'
                            ]) }}
                            <div class="col-md-6">
                                {{ Form::password('password', [
                                    'class'    => 'form-control',
                                    'required' => 'required',
                                ]) }}
                                @if ($errors->has('password'))
                                    <span class="error text-danger">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('password_confirmation', Lang::get('form.password_confirmation'), [
                                'class' => 'col-md-4 col-form-label text-md-right'
                            ]) }}
                            <div class="col-md-6">
                                {{ Form::password('password_confirmation', [
                                    'class'    => 'form-control',
                                    'required' => 'required',
                                ]) }}
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{ Form::submit(Lang::get('form.register'), ['class' => 'btn btn-primary']) }}
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
