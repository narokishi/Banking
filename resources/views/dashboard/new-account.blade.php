@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ Lang::get('dashboard.request_account') }}</div>
                <div class="card-body">
                    {{ Form::open(['route' => 'account.store']) }}
                        <div class="form-group row">
                            {{ Form::label('currency', Lang::get('form.currency'), [
                                'class' => 'col-md-4 col-form-label text-md-right'
                            ]) }}
                            <div class="col-md-6">
                                {{ Form::select('currency', $currencies, null, [
                                    'class'    => 'form-control',
                                    'required' => 'required',
                                ]) }}
                                @if ($errors->has('currency'))
                                    <span class="error text-danger">
                                        <strong>{{ $errors->first('currency') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row">
                            {{ Form::label('type', Lang::get('form.account_type'), [
                                'class' => 'col-md-4 col-form-label text-md-right'
                            ]) }}
                            <div class="col-md-6">
                                {{ Form::select('type', $types, null, [
                                    'class'    => 'form-control',
                                    'required' => 'required',
                                ]) }}
                                @if ($errors->has('type'))
                                    <span class="error text-danger">
                                        <strong>{{ $errors->first('type') }}</strong>
                                    </span>
                                @endif
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-6 offset-md-4">
                                {{ Form::submit(Lang::get('form.send'), ['class' => 'btn btn-primary']) }}
                            </div>
                        </div>
                    {{ Form::close() }}
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
