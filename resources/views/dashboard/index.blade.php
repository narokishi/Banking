@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">{{ Lang::get('dashboard.dashboard') }}</div>
                <div class="card-body">
                    <div class="row">
                        @foreach($user->basics as $account)
                            <div class="col-md-2 col-3">
                                <img src="{{ asset('images/basic.svg') }}" alt=""/>
                            </div>
                            <div class="col-md-10 col-9">
                                <div class="form-group">
                                    <strong>{{ Lang::get('account.type.'.$account->type) }}</strong><br/>
                                    {{ $account->iban }}<br/>
                                    <a href="#" class="btn-link">{{ Lang::get('dashboard.history') }}</a><br/>
                                    <a href="#" class="btn-link">{{ Lang::get('dashboard.make_transaction') }}</a><br/>
                                    <a href="#" class="btn-link">{{ Lang::get('dashboard.order_card') }}</a><br/>
                                    <a href="#" class="btn-link">{{ Lang::get('dashboard.representative') }}</a><br/>
                                </div>
                            </div>
                        @endforeach
                        @foreach($user->savings as $account)
                            <div class="col-md-2 col-3">
                                <img src="{{ asset('images/saving.svg') }}" alt=""/>
                            </div>
                            <div class="col-md-10 col-9">
                                <div class="form-group">
                                    <strong>{{ Lang::get('account.type.'.$account->type) }}</strong><br/>
                                    {{ $account->iban }}<br/>
                                    <a href="#" class="btn-link">{{ Lang::get('dashboard.history') }}</a><br/>
                                    <a href="#" class="btn-link">{{ Lang::get('dashboard.make_transaction') }}</a><br/>
                                    <a href="#" class="btn-link">{{ Lang::get('dashboard.change_type') }}</a><br/>
                                    <a href="#" class="btn-link">{{ Lang::get('dashboard.representative') }}</a><br/>
                                </div>
                            </div>
                        @endforeach
                    </div>
                    <a class="btn btn-link" href="#">{{ Lang::get('dashboard.fill_data') }}</a><br/>
                    <a class="btn btn-link" href="{{ route('account.create') }}">{{ Lang::get('dashboard.request_account') }}</a>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
