<?php

return [
    'currency' => [
        'pln' => 'Polish zloty',
        'eur' => 'Euro',
    ],
    'type'     => [
        'basic'  => 'Basic Account',
        'saving' => 'Savings Account',
    ],
];
