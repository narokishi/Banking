<?php

return [
    'login'                 => 'Login',
    'register'              => 'Register',
    'email'                 => 'E-Mail Address',
    'password'              => 'Password',
    'password_confirmation' => 'Confirm password',
    'password_reset'        => 'Reset password',
    'remember'              => 'Remember me',
    'forgot'                => 'Forgot Your Password?',
    'name'                  => 'Name',
    'first_name'            => 'First name',
    'last_name'             => 'Last name',
    'send'                  => 'Send',
    'currency'              => 'Currency',
    'account_type'          => 'Account type',
];
