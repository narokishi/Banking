<?php

return [
    'dashboard'        => 'Dashboard',
    'fill_data'        => 'Fill personal data',
    'request_account'  => 'Request new account',
    'history'          => 'History',
    'make_transaction' => 'Make new transaction',
    'order_card'       => 'Order new card',
    'representative'   => 'Representatives',
    'change_type'      => 'Change account type',
];
