<?php

return [
    'password'   => 'password',
    'first_name' => 'first name',
    'last_name'  => 'last name',
    'email'      => 'email',
    'currency'   => 'currency',
    'type'       => 'account type',
];
