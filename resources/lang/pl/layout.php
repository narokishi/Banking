<?php

return [
    'login'     => 'Zaloguj',
    'register'  => 'Zarejestruj',
    'logout'    => 'Wyloguj',
    'dashboard' => 'Panel użytkownika',
];
