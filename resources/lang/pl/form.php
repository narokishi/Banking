<?php

return [
    'login'                 => 'Zaloguj',
    'register'              => 'Zarejestruj',
    'email'                 => 'Adres e-mail',
    'password'              => 'Hasło',
    'password_confirmation' => 'Potwierdź hasło',
    'password_reset'        => 'Przywróć hasło',
    'remember'              => 'Zapamiętaj mnie',
    'forgot'                => 'Zapomniałeś hasła?',
    'name'                  => 'Imię i nazwisko',
    'first_name'            => 'Imię',
    'last_name'             => 'Nazwisko',
    'send'                  => 'Wyślij',
    'currency'              => 'Waluta',
    'account_type'          => 'Typ konta',
];
