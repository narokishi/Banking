<?php

return [
    'dashboard'        => 'Panel użytkownika',
    'fill_data'        => 'Uzupełnij dane osobowe',
    'request_account'  => 'Złóż wniosek o konto',
    'history'          => 'Historia',
    'make_transaction' => 'Wykonaj transakcję',
    'order_card'       => 'Zamów kartę',
    'representative'   => 'Pełnomocnicy',
    'change_type'      => 'Zmień typ konta',
];
