<?php

return [
    'password'   => 'hasło',
    'first_name' => 'imię',
    'last_name'  => 'nazwisko',
    'email'      => 'email',
    'currency'   => 'waluta',
    'type'       => 'typ konta',
];
