<?php

return [
    'currency' => [
        'pln' => 'Złoty',
        'eur' => 'Euro',
    ],
    'type'     => [
        'basic'  => 'Konto podstawowe',
        'saving' => 'Konto oszczędnościowe',
    ],
];
