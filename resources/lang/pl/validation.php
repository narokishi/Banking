<?php

return [
    'confirmed' => 'Potwierdzenie pola :attribute się nie zgadza.',
    'email'     => 'Pole :attribute musi być poprawnym adresem e-mail.',
    'required'  => 'Pole :attribute jest wymagane.',
    'string'    => 'Pole :attribute musi być strunowe.',
    'max'       => [
        'string' => 'Pole :attribute musi być krótsze niż :max znaków.',
    ],
    'min'       => [
        'string' => 'Pole :attribute musi być dłuższe niż :min znaków.',
    ],
    'unique'    => 'Podany :attribute jest już zajęty.',
];
