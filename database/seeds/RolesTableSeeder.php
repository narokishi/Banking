<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    private $roles = [
        [
            'id'   => 1,
            'name' => 'admin',
        ],
        [
            'id'   => 2,
            'name' => 'employee',
        ],
        [
            'id'   => 3,
            'name' => 'client',
        ]
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert($this->roles);
    }
}
