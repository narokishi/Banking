<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    private $permissions = [
        // Admin
        1 => [
            //
        ],
        // Employee
        2 => [
            [
                'name'  => 'admin.account.accept',
                'allow' => true,
            ],
            [
                'name'  => 'admin.user.delete',
                'allow' => true,
            ],
        ],
        // Client
        3 => [
            //
        ],
    ];

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $permissions = collect($this->permissions)->map(function ($permission, $role) {
            return collect($permission)->map(function ($permission) use ($role) {
                $permission['role_id'] = $role;

                return $permission;
            });
        })->flatten(1)->toArray();

        DB::table('permissions')->insert($permissions);
    }
}
