<?php

Route::name('lang')->get('lang/{language}', 'LanguageController');

Route::name('welcome')->get('/', 'PageController@welcome');

Route::resource('user', 'UserController', [
    'only' => ['store'],
]);

Auth::routes();
