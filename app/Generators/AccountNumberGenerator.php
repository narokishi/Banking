<?php

namespace App\Generators;

use Illuminate\Support\Facades\Config;
use Faker\Factory as Faker;

class AccountNumberGenerator
{
    private $countryCode;

    private $prefix;

    public function __construct()
    {
        $this->countryCode = Config::get('app.bank_alpha2');
        $this->prefix      = Config::get('app.bank_prefix');
    }

    public function __toString()
    {
        return $this->make();
    }

    public function make()
    {
        return Faker::create()->unique()->iban($this->countryCode, $this->prefix);
    }
}
