<?php

namespace App\Providers;

use App\User;
use Illuminate\Support\Collection;
use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        $this->defineGates();

        //
    }

    private function defineGates()
    {
        Collection::make([
            'admin.account.accept',
        ])->each(function ($gate) {
            $this->defineGate($gate);
        });
    }

    private function defineGate($name)
    {
        Gate::define($name, function (User $user) use ($name) {
            return $user->permissions->pluck('allow', 'name')->get($name);
        });
    }
}
