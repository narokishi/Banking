<?php

namespace App;

use App\Traits\HasIbanAttribute;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class Account extends Model
{
    /**
     * Traits
     */
    use SoftDeletes;
    use HasIbanAttribute;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'currency', 'type', 'user_id', 'balance', 'iban',
    ];

    /**
     * The model's attributes.
     *
     * @var array
     */
    protected $attributes = [
        'balance'   => 0,
        'is_active' => false,
    ];

    /**
     * The attributes that should be mutated to dates.
     *
     * @var array
     */
    protected $dates = [
        'deleted_at',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'is_active' => 'boolean',
    ];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function scopeOnlyActive($query)
    {
        $query->where('is_active', true);
    }

    public function scopeWithoutActive($query)
    {
        $query->where('is_active', false);
    }
}
