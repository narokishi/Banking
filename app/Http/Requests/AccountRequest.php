<?php

namespace App\Http\Requests;

use App\Generators\AccountNumberGenerator as Generator;
use Illuminate\Foundation\Http\FormRequest;
use App\Traits\HasTranslateAttributes;
use Illuminate\Support\Facades\Auth;

class AccountRequest extends FormRequest
{
    use HasTranslateAttributes;

    public $translateAttributes = [
        'type', 'currency',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type'     => 'required|string',
            'currency' => 'required|string',
        ];
    }

    public function getAccount()
    {
        return array_merge(
            $this->only('currency', 'type'),
            $this->getAccountNumber()
        );
    }

    private function getAccountNumber()
    {
        return [
            'iban' => new Generator
        ];
    }
}
