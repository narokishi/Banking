<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use App\Traits\HasTranslateAttributes;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class UserRequest extends FormRequest
{
    use HasTranslateAttributes;

    public $translateAttributes = [
        'first_name', 'last_name', 'email', 'password',
    ];

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::guest();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:50',
            'last_name'  => 'required|string|max:50',
            'email'      => 'required|string|max:255|email|unique:users',
            'password'   => 'required|string|min:6|confirmed',
        ];
    }

    public function getUser()
    {
        return array_merge(
            $this->only(['first_name', 'last_name', 'email']),
            ['password' => Hash::make($this->input('password'))]
        );
    }
}
