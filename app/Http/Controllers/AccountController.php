<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Lang;
use Illuminate\Support\Facades\Auth;
use App\Http\Requests\AccountRequest;

class AccountController extends Controller
{
    public function create()
    {
        $currencies = Lang::get('account.currency');
        $types      = Lang::get('account.type');

        return view('dashboard.new-account', [
            'currencies' => $currencies,
            'types'      => $types,
        ]);
    }

    public function store(AccountRequest $request)
    {
        Auth::user()
            ->accounts()
            ->create($request->getAccount());

        return redirect()
            ->route('dashboard');
    }
}