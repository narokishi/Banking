<?php

namespace App\Http\Controllers;

use Illuminate\Support\Facades\Auth;

class PageController extends Controller
{
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        $user = Auth::user();

        return view('dashboard.index', [
            'user' => $user,
        ]);
    }

    public function welcome()
    {
        return view('welcome');
    }
}
