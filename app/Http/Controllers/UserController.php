<?php

namespace App\Http\Controllers;

use App\User;
use App\Http\Requests\UserRequest;
use Illuminate\Support\Facades\Auth;

class UserController extends Controller
{
    public function store(UserRequest $request)
    {
        $user = User::create($request->getUser());

        Auth::guard()->login($user);

        return redirect()->route('dashboard');
    }
}
