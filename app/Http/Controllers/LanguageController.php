<?php

namespace App\Http\Controllers;

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Config;
use Illuminate\Support\Facades\Session;

class LanguageController
{
    public function __invoke($language)
    {
        if(!Arr::exists(Config::get('app.languages'), $language)) {
            $language = Config::get('app.fallback_locale');
        }

        Session::put('locale', $language);

        return redirect()->back();
    }
}
