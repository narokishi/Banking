<?php

namespace App\Http\Controllers\Admin;

class AccountController extends Controller
{
    /**
     * Gate prefix given to method by controller.
     *
     * @var string
     */
    protected $controllerGatePrefix = 'account';

    /**
     * @todo Make method to accept user account requests.
     */
    public function accept()
    {
        //
    }
}