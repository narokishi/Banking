<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Facades\Gate;
use App\Http\Controllers\Controller as AppController;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;

class Controller extends AppController
{
    /**
     * Route name to redirect unauthorized users.
     *
     * @var string
     */
    protected $redirectRoute = 'dashboard';

    /**
     * Gate prefix given to method by namespace.
     *
     * @var string
     */
    protected $namespaceGatePrefix = 'admin';

    /**
     * Gate prefix given to method by controller.
     *
     * @var string
     */
    protected $controllerGatePrefix = '';

    /**
     * Execute an action on the controller.
     *
     * @param  string  $method
     * @param  array   $parameters
     * @throws \Symfony\Component\HttpKernel\Exception\NotFoundHttpException
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function callAction($method, $parameters)
    {
        if (Gate::allows($this->getGateName($method))) {
            return parent::callAction($method, $parameters);
        } else {
            throw new NotFoundHttpException;
        }
    }

    /**
     * Get full name of the called method.
     *
     * @param  string $method
     * @return string
     */
    private function getGateName($method)
    {
        return sprintf('%s.%s.%s',
            $this->namespaceGatePrefix,
            $this->controllerGatePrefix,
            $method
        );
    }
}