<?php

namespace App\Traits;

use Illuminate\Support\Facades\Lang;

trait HasTranslateAttributes
{
    public function attributes()
    {
        $attributes = [];

        if (!empty($this->translateAttributes)) {
            foreach($this->translateAttributes as $attribute) {
                $attributes[$attribute] = Lang::get("attribute.$attribute");
            }
        }

        return $attributes;
    }
}
