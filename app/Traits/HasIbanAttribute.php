<?php

namespace App\Traits;

use Illuminate\Support\Str;

trait HasIbanAttribute
{
    private $iban;

    public function getIbanAttribute($iban)
    {
        if (empty($iban)) {
            return null;
        }

        $this->iban = $iban;

        $chunks[] = $this->pull(0, 2);
        $chunks[] = $this->pull(0, 2);

        while ($this->iban) {
            $chunks[] = $this->pull(0, 4);
        }

        return implode(' ', $chunks);
    }

    private function pull($start, $length = null)
    {
        $chunk      = Str::substr($this->iban, $start, $length);
        $this->iban = Str::substr($this->iban, $length);

        return $chunk;
    }
}
